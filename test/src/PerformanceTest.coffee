


module.exports = class PerformanceTest
	constructor: (name, cb, iterations)->

		@times = []
		@cb = cb
		@iterations = iterations
		@iterateCallbacks()
		@outputResult()


	iterateCallbacks: ()->
		for i in [0...@iterations] by 1
			@times.push(@timeCallback())

	outputResult: ()->
		console.log name, @calcAverageTime(@times)


	calcAverageTime: ()->
		t = 0
		n = @times.length
		for time in @times
			t += time

		return t / n

	timeCallback: ()->
		t0 = performance.now()
		@cb()
		t1 = performance.now()
		return t1 - t0