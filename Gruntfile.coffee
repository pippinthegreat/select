module.exports = (grunt)->
	grunt.initConfig
		pkg: grunt.file.readJSON 'package.json'
		watch:
			main:
				files: ['test/src/**/*', 'src/**/*']
				tasks: ['coffeeify']
				options:
					atBegin: true


		coffeeify:
			build:
				cwd:  'test/src'
				src:  ['*.coffee']
				dest: 'test/public/'


	grunt.loadNpmTasks 'grunt-contrib-watch'
	grunt.loadNpmTasks 'grunt-contrib-coffeeify'

	grunt.registerTask 'default', ['watch']

