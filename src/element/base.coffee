

class Element

	constructor: (selector)->
		@length = 0

		@_init(selector)


	_init: (selector)->
		if !selector?
			return @

		if selector.nodeType
			# selector is already an element
			@_attachElement(selector)
		else if typeof selector isnt 'string' and !(selector instanceof Element)
			# selector is an array of elements
			@_attachElements(selector)
		else if selector instanceof Element
			# selector is already a Select object
			@_attachElements(selector)
			@selector = selector.selector
		else
			@_classifyElements(Select.findElement(selector))
			@selector = selector

		@_checkForMultipleElements()
		return @



module.exports = Element