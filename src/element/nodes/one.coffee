


module.exports = (Element, Select)->

	Element::_eachOne = (cb)->
		cb.call(@, 0, @[0])
		return @

	Element::_eachNodeOne = (cb)->
		cb(@[0])
		return @

	Element::_findNodesOne = (cb, unique = false)->
		arr = [cb(@[0])]
		return arr

	Element::_findNodesFromArrayOne = (cb, unique = false)->
		arr = []
		r = cb(@[0], arr)
		for item in r
			arr.push(item)
		return arr

	Element::_evaluateNodesOne = (cb)->
		return cb(@[0])