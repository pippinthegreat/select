


module.exports = (Element, Select)->

	Element::_eachMany = (cb)->
		for i in [0...@length] by 1
			cb.call(@, i, @[i])
		return @

	Element::_eachNodeMany = (cb)->
		# TODO check at init - if length is 1, use @firstNode
		l = @length
		while l--
			cb(@[l])
		return @

	Element::_findNodesMany = (cb, unique = false)->
		arr = []
		l = @length
		while l--
			r = cb(@[l])
			if (unique and r not in arr) or !unique
				arr.push(r)
		return arr

	Element::_findNodesFromArrayMany = (cb, unique = false)->
		arr = []
		l = @length
		while l--
			r = cb(@[l], arr)
			if (unique and r not in arr) or !unique
				for item in r
					arr.push(item)
		return arr

	Element::_evaluateNodesMany = (cb)->
		l = @length
		while l--
			if cb(@[l]) then return true
		return false