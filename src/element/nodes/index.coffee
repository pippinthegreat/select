


module.exports = (Element, Select)->
	require('./many')(Element, Select)
	require('./one')(Element, Select)

	Element::_checkForMultipleElements = ->
		if @length <= 1
			@each = @_eachOne
			@eachNode = @_eachNodeOne
			@findNodes = @_findNodesOne
			@findNodesFromArray = @_findNodesFromArrayOne
			@evaluateNodes = @_evaluateNodesOne
		else
			@each = @_eachMany
			@eachNode = @_eachNodeMany
			@findNodes = @_findNodesMany
			@findNodesFromArray = @_findNodesFromArrayMany
			@evaluateNodes = @_evaluateNodesMany


	Element::_classifyElements = (elements)->
		l = elements.length 
		if l?
			@_attachElements(elements)
		else
			@_attachElement(elements)

	Element::_attachElements = (elements)->
		l = elements.length 
		@length = l
		for el, i in elements
			@[i] = el

	Element::_attachElement = (element)->
		@length = 1
		@[0] = element

	Element::get = (index)->
		return @[index]
