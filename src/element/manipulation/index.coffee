
module.exports = (Element, Select)->

	require('./after')(Element, Select)
	require('./before')(Element, Select)
	require('./append')(Element, Select)
	require('./prepend')(Element, Select)


	Element::clone = ->
		clones = @findNodes((el)-> return el.cloneNode(true))
		return new Element(clones)

	Element::empty = ->
		@eachNode((el)->
			el.innerHTML = ''
		)
		return @

	Element::remove = ->
		@eachNode((el)->
			el.parentNode.removeChild(el)
		)
		return @

	Element::replaceWith = (htmlString)->
		@eachNode((el)->
			el.outerHTML = htmlString
		)
		return @

	Element::html = (htmlString)->
		if htmlString?
			@eachNode((el)->
				el.innerHTML = htmlString
			)
			return @
		else
			return @[0].innerHTML

	Element::text = (str)->
		if str?
			@eachNode((el)->
				el.textContent = str
			)
			return @
		else
			return @[0].textContent