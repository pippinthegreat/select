


module.exports = (Element, Select)->

	Element::_insertHTMLBefore = (string)->
		@eachNode((el)->
			el.insertAdjacentHTML('beforebegin', string)
		)

	Element::_insertElementBefore = (beforeElements)->
		@eachNode((el)->
			beforeElements.eachNode((beforeEl)->
				el.parentNode.insertBefore(beforeEl.cloneNode(true), el)
			)
		)

	Element::before = (beforeNode)->
		if typeof beforeNode is 'string'
			@_insertHTMLBefore(beforeNode)
		else if beforeNode instanceof Element
			@_insertElementBefore(beforeNode)
		return @