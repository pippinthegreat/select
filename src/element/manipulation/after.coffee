


module.exports = (Element, Select)->

	Element::_insertHTMLAfter = (string)->
		@eachNode((el)->
			el.insertAdjacentHTML('afterend', string)
		)

	Element::_insertElementAfter = (afterElements)->
		@eachNode((el)->
			afterElements.eachNode((afterEl)->
				el.parentNode.insertBefore(afterEl.cloneNode(true), el.nextSibling)
			)
		)

	Element::after = (afterNode)->
		if typeof afterNode is 'string'
			@_insertHTMLAfter(afterNode)
		else if afterNode instanceof Element
			@_insertElementAfter(afterNode)
		return @