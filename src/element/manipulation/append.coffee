


module.exports = (Element, Select)->

	Element::_appendHTML = (string)->
		@eachNode((el)->
			el.insertAdjacentHTML('beforeend', string)
		)

	Element::_appendElement = (appendElements)->
		@eachNode((el)->
			appendElements.eachNode((appendEl)->
				el.appendChild(appendEl.cloneNode(true))
			)
		)

	Element::append = (appendNode)->
		if typeof appendNode is 'string'
			@_appendHTML(appendNode)
		else if appendNode instanceof Element
			@_appendElement(appendNode)
		return @