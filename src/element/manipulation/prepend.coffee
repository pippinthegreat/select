


module.exports = (Element, Select)->

	Element::_prependHTML = (string)->
		@eachNode((el)->
			el.insertAdjacentHTML('afterbegin', string)
		)

	Element::_prependElement = (prependElements)->
		@eachNode((el)->
			prependElements.eachNode((prependEl)-> 
				el.insertBefore(prependEl.cloneNode(true), el.firstChild)
			)
		)

	Element::prepend = (prependNode)->
		if typeof prependNode is 'string'
			@_prependHTML(prependNode)
		else if prependNode instanceof Element
			@_prependElement(prependNode)
		return @