


module.exports = (Element, Select)->
	
	Element::addClass = (className)->
		return @eachNode((el)-> if !el.classList.contains(className) then el.classList.add(className))


	Element::removeClass = (className)->
		return @eachNode((el)-> if !el.classList.contains(className) then el.classList.remove(className))


	Element::toggleClass = (className)->
		return @eachNode((el)-> el.classList.toggle(className))
			

	Element::hasClass = (className)->
		return @evaluateNodes((el)-> return el.classList.contains(className))
