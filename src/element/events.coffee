
module.exports = (Element, Select)->

	splitEventString = (eventString)->
		str = eventString.trim()
		return str.split(/\s/)

	ALL_EVENTS = 'blur focus focusin focusout load resize scroll unload click dblclick ' +
		'mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave ' +
		'change select submit keydown keypress keyup error contextmenu'

	SPLIT_EVENTS = splitEventString(ALL_EVENTS)
	

	Element::on = (eventString, eventHandler)->
		events = splitEventString(eventString)

		for eventName in events
			@eachNode((el)-> el.addEventListener(eventName, eventHandler, false))

		return @

	Element::off = (eventString, eventHandler)->
		if typeof eventString is 'function'
			events = SPLIT_EVENTS
			eventHandler = eventString
		else
			events = splitEventString(eventString)

		for eventName in events
			@eachNode((el)-> el.removeEventListener(eventName, eventHandler))

		return @


	Element::trigger = (eventName)->
		event = document.createEvent('HTMLEvents')
		event.initEvent(eventName, true, false)
		@eachNode((el)-> el.dispatchEvent(event))

		return @

	Element::triggerCustom = (eventName, data)->
		if window.CustomEvent
			event = new CustomEvent(eventName, detail: data)
		else 
			event = document.createEvent('CustomEvent')
			event.initCustomEvent(eventName, true, true, data)

		@eachNode((el)-> el.dispatchEvent(event))

		return @

	Element::ready = (readyHandler)->
		if document.readyState isnt 'loading'
			readyHandler()
		else
			document.addEventListener('DOMContentLoaded', readyHandler)

		return @