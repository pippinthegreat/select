


module.exports = (Element, Select)->

	Element::filter = (cb)->
		filtered = Array.prototype.filter.call(@, cb)
		return new Element(filtered)

	Element::matches = (el, otherEl)->
		return el is otherEl

	Element::matchesSelector = (el, selector)->
		return (el.matches or
				el.matchesSelector or
				el.msMatchesSelector or
				el.mozMatchesSelector or
				el.webkitMatchesSelector or
				el.oMatchesSelector).call(el, selector)

	Element::is = (selector)->
		if typeof selector is 'string'
			return @evaluateNodes((el)=>
				return @matchesSelector(el, selector)
			)
		else
			return @evaluateNodes((el)=>
				return @matches(el, selector.get(0))
			)

	Element::find = (selector)->
		if Select.isClass(selector)
			found = @findNodesFromArray((el)->
				return Select.findClass(selector, el)
			, true)
		else
			found = @findNodesFromArray((el)->
				return Select.findAllOfSelector(selector, el)
			, true)
		return new Element(found)

	Element::attr = (attr, value)->
		if value?
			@eachNode((el)-> el.setAttribute(attr, value))
			return @
		else
			return @[0].getAttribute(attr)

	Element::removeAttr = (attr)->
		@eachNode((el)-> el.removeAttribute(attr))
		return @