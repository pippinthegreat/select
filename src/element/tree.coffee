


module.exports = (Element, Select)->

	Element::children = ->
		children = Select.flattenOnce(@findNodes((el)->
			return Select.toArray(el.children)
		))
		return new Element(children)

	Element::siblings = ->
		siblings = Select.flattenOnce(@findNodesFromArray((el, arr)->
			return Array.prototype.filter.call(el.parentNode.children, (child)=>
				return child isnt el and el not in arr
			)
		, true))
		return new Element(siblings)

	Element::next = ->
		return new Element(@[0].nextElementSibling)

	Element::prev = ->
		return new Element(@[0].previousElementSibling)

	Element::parent = ->
		parent = @findNodes((el)->
			return el.parentNode
		, true)
		return new Element(parent)

	Element::parents = (selector)->
		el = @[0]
		parents = @findNodesFromArray((el, arr)=>
			temp = []
			p = el.parentNode
			while p isnt document
				o = p
				if o not in arr
					if selector?
						if @matchesSelector(o, selector)
							temp.push(o)
					else
						temp.push(o)
				p = o.parentNode
			return temp
		, true)

		return new Element(parents)
