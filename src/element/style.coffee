
module.exports = (Element, Select)->

	Element::position = (relativeToViewport)->
		if relativeToViewport?
			return @[0].getBoundingClientRect()
		else
			return {
				left: @[0].offsetLeft
				top: @[0].offsetTop
			}

	Element::offset = ->
		rect = @[0].getBoundingClientRect()

		return {
			top: rect.top + document.body.scrollTop
			left: rect.left + document.body.scrollLeft
		}

	Element::offsetParent = ->
		return @[0].offsetParent or @[0]

	Element::outerHeight = (withMargin)->
		if withMargin?
			height = @[0].offsetHeight
			style = getComputedStyle(@[0])

			height += parseInt(style.marginTop) + parseInt(style.marginBottom)
			return height
		else
			return @[0].offsetHeight

	Element::outerWidth = (withMargin)->
		if withMargin?
			width = @[0].offsetWidth
			style = getComputedStyle(@[0])

			width += parseInt(style.marginLeft) + parseInt(style.marginRight)
			return width
		else
			return @[0].offsetWidth

	Element::_cssString = (styles)->
		str = ';'
		for style, value of styles
			str += style
			str += ':'
			str += value
			str += ';'

		@eachNode((el)->
			el.style.cssText += str
		)


	Element::css = (styles, value)->
		if typeof styles is 'string' and !value
			return getComputedStyle(@[0])[styles]
		else if Select.type(styles) is 'string' and value
			@eachNode((el)->
				el.style.cssText += ';' + styles + ':' + value + ';'
			)
		else
			@_cssString(styles)
		return @
			

	Element::height = (height)->
		if height?
			if typeof height isnt 'string'
				height = height.toString()
				height += 'px'
			@[0].style.height = height
			return @
		else
			return parseInt(getComputedStyle(@[0]).height) or 0

	Element::width = (width)->
		if width?
			if typeof width isnt 'string'
				width = width.toString()
				width += 'px'
			@[0].style.width = width
			return @
		else
			return parseInt(getComputedStyle(@[0]).width) or 0