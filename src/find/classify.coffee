

module.exports = (Select)->
	MATCHES =
		ID: /^(\#)([A-Za-z]{1,2})([0-9A-Za-z\-\_]*)$/
		CLASS: /^(\.)([A-Za-z]{1,2})([0-9A-Za-z\-\_]*)$/
		TAG: /^([0-9A-Za-z]+)$/
		HTML: /^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$/

	Select.findElement = (selector)->
		if Select.isID(selector)
			return Select.findID(selector)

		else if Select.isClass(selector)
			return Select.findClass(selector)

		else if Select.isTag(selector)
			return Select.findTag(selector)

		else if Select.isHTML(selector)
			# TODO html
			return Select.createHTML(selector)

		else
			return Select.findAllOfSelector(selector)

	Select.isID = (selector)->
		return if selector.search(MATCHES.ID) isnt -1 then true else false

	Select.isClass = (selector)->
		return if selector.search(MATCHES.CLASS) isnt -1 then true else false

	Select.isTag = (selector)->
		return if selector.search(MATCHES.TAG) isnt -1 then true else false

	Select.isHTML = (selector)->
		return if selector.search(MATCHES.HTML) isnt -1 then true else false

