

module.exports = (Select)->

	cleanSelector = (selector)->
		if selector[0] = '.'
			return trimSelector(selector)
		else
			return selector

	trimSelector = (selector)->
		return selector.slice(1)


	Select.findID = (id)->
		id = cleanSelector(id)
		return document.getElementById(id)

	Select.findClass = (className, el = document)->
		className = cleanSelector(className)
		return el.getElementsByClassName(className)

	Select.findTag = (tag, el = document)->
		return el.getElementsByTagName(tag)

	Select.findFirstOfSelector = (selector, el = document)->
		return el.querySelector(selector)

	Select.findAllOfSelector = (selector, el = document)->
		return el.querySelectorAll(selector)