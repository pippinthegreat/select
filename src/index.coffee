

###

	Select

###



# base objects
Element = require './element/base'
Select = require './utils/factory'


# static utility methods
require('./utils/default')(Select)
require('./utils/extend')(Element, Select)
require('./utils/dom')(Select)

# static find methods
require('./find/classify')(Select)
require('./find/query')(Select)

# public element methods
require('./element/nodes/')(Element, Select)
require('./element/manipulation/')(Element, Select)
require('./element/utils')(Element, Select)
require('./element/tree')(Element, Select)
require('./element/style')(Element, Select)
require('./element/classes')(Element, Select)
require('./element/events')(Element, Select)



# export
window.Select = Select
window.$ = Select
module.exports = Select
