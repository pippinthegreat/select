

module.exports = (Select)->

	Select.blur = ->
		document.activeElement.blur()

	Select.parseHTML = (htmlString)->
		tmp = document.implementation.createHTMLDocument()
		tmp.body.innerHTML = htmlString
		return tmp.body.children

	Select.contains = (el, child)->
		return el isnt child and el.contains(child)

	Select.containsSelector = (el, selector)->
		return el.querySelector(selector) isnt null
