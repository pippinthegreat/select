

module.exports = (Select)->

	Select.flattenOnce = (arr)->
		return [].concat(arr...)

	Select.toArray = (collection)->
		# standardize htmlcollections etc
		return Array.prototype.slice.call(collection)
