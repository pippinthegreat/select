var static = require('node-static');
var port = process.env.PORT || 3000;
var fileServer = new static.Server('./test/public');

require('http').createServer(function (request, response) {
    request.addListener('end', function () {
        fileServer.serve(request, response);
    }).resume();
}).listen(port);